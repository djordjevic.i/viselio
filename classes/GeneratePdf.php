<?php
require 'vendor/autoload.php';
use Dompdf\Dompdf;

Class GeneratePdf {

    public function __construct($content)
    {
        $this->content = $content;
    }

    public function downloadFile()
    {
        $dompdf = new Dompdf();
        $dompdf->loadHtml($this->content);
        $dompdf->setPaper('A2', 'landscape');
        $dompdf->render();
        $dompdf->stream();
    }
}