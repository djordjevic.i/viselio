function loadData (config) {
    this.url = config.url;
    this.gridId = config.grid;
    this.getInfo = function() {
        $(this.gridId).DataTable({
            "ajax":  this.url,
            "columns": [
                { "data": "citizenship" },
                { "data": "destination" },
                { "data": "docDescription" },
                { "data": "docNote" },
                { "data": "docIdentifier" },
                { "data": "docType" },
                { "data": "docId" },
                { "data": "entryType" },
                { "data": "noteVisaNotRequired" },
                { "data": "updateAt" },
                { "data": "visaType" },
            ],
        });
    };
}

$(document).ready( function () {
    visaData.getInfo();
    });